# ************************************************************
# Sequel Pro SQL dump
# Версия 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: hormold.ru (MySQL 5.5.33)
# Схема: pho1
# Время создания: 2014-03-23 18:29:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы bad_albums
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bad_albums`;

CREATE TABLE `bad_albums` (
  `album_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`album_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Дамп таблицы cities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `city_title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Дамп таблицы groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `group_screen_name` varchar(256) NOT NULL,
  `last_update` datetime NOT NULL,
  `is_private` int(11) DEFAULT '0',
  `parse_albums` int(11) DEFAULT '1',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Дамп таблицы groups_to_parse
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_to_parse`;

CREATE TABLE `groups_to_parse` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_mc` int(11) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Дамп таблицы hits
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hits`;

CREATE TABLE `hits` (
  `uid` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Дамп таблицы invites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invites`;

CREATE TABLE `invites` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(64) DEFAULT NULL,
  `sended` int(11) NOT NULL DEFAULT '0',
  `code` varchar(26) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Дамп таблицы main
# ------------------------------------------------------------

DROP TABLE IF EXISTS `main`;

CREATE TABLE `main` (
  `pid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `src` varchar(256) NOT NULL,
  `src_preview` varchar(256) NOT NULL,
  `src_thumb` varchar(256) NOT NULL,
  `wall_id` int(11) DEFAULT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`),
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Дамп таблицы sazha1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sazha1`;

CREATE TABLE `sazha1` (
  `photo_id` int(11) NOT NULL,
  `longip` int(11) NOT NULL,
  `type` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`photo_id`,`longip`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Дамп таблицы sugg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sugg`;

CREATE TABLE `sugg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(100) DEFAULT NULL,
  `done` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Дамп таблицы users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `display_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sex` int(1) DEFAULT NULL,
  `photo_50` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `photo_100` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `photo_200` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_200_orig` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `cache_has_photos` int(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `city_id` (`city_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Дамп таблицы votes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `votes`;

CREATE TABLE `votes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `ip` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
